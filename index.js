/**
 * @format
 */
import Login from "./src/components/auth/Login";
import Profile from "./src/components/views/Profile";
import Home from "./src/components/views/Home";
import Map from "./src/components/views/Map";
import UpdateProfile from "./src/components/views/UpdateProfile";
import Drawer from "./src/components/ui/Drawer";
import { Navigation } from "react-native-navigation";

import { AsyncStorage } from "react-native";
Navigation.registerComponent(`nogard.Auth.Login`, () => Login);
Navigation.registerComponent(`nogard.Views.Home`, () => Home);
Navigation.registerComponent(`nogard.Views.Map`, () => Map);
Navigation.registerComponent(`nogard.Views.Profile`, () => Profile);
Navigation.registerComponent(`nogard.Views.UpdateProfile`, () => UpdateProfile);
Navigation.registerComponent(`nogard.UI.Drawer`, () => Drawer);

// console.warn("tokenAsync", token);

Navigation.events().registerAppLaunchedListener(() => {
  Navigation.setRoot({
    root: {
      component: {
        name: "nogard.Auth.Login"
      }
    }
  });
});
// Navigation.events().registerAppLaunchedListener(() => {
//   Navigation.setRoot({
//     root: {
//       sideMenu: {
//         left: {
//           component: {
//             name: "nogard.UI.Drawer"
//           }
//         },
//         center: {
//           component: {
//             name: "nogard.Views.Home",
//             id: 'home'
//           }
//         }
//       }
//     }
//   });
// });
