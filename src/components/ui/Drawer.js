import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import { Navigation } from "react-native-navigation";

import { Container, Content, Button } from "native-base";
export default class Drawer extends Component {
  homeDrawer = () => {
    // Navigation.setRoot({
    //   root: {
    //     sideMenu: {
    //       left: {
    //         component: {
    //           name: "nogard.UI.Drawer"
    //         }
    //       },
    //       center: {
    //         component: {
    //           name: "nogard.Views.Home",
    //           options: {
    //             topBar: {
    //               visible: false,
    //               height: 0
    //             }
    //           }
    //         }
    //       }
    //     }
    //   }
    // });
  };

  visibleDrawer = () => {
    Navigation.mergeOptions("home", {
      sideMenu: {
        left: {
          visible: false
        }
      }
    });
    this.profileDrawer();
  };
  profileDrawer = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nogard.Views.Profile",
              options: {
                topBar: {
                  visible: false,
                  height: 0
                }
              }
            }
          }
        ]
      }
    });
  };
  render() {
    return (
      <Container>
        <Content>
          <Button full warning onPress={() => this.homeDrawer()}>
            <Text>Home</Text>
          </Button>
          <Button full warning onPress={() => this.visibleDrawer()}>
            <Text>Profile</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

// const styles = StyleSheet.create({});
