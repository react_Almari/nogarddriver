/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import {
  Item,
  Input,
  Container,
  Header,
  Left,
  Body,
  Content,
  Button,
  Icon,
  Right,
  Title
} from "native-base";
import { Navigation } from "react-native-navigation";
const axios = require("axios");
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      address: "",
      dateOfBirth: ""
    };
    this.navigationEventListener = Navigation.events().bindComponent(this);
  }
  componentWillUnmount() {
    // Not mandatory
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
  }
  async componentDidAppear() {
    const self = this;
    const token = await AsyncStorage.getItem("token");
    axios.defaults.headers.common = { Authorization: token };
    axios
      .get("http://192.168.31.42:3100/v1/drivers/loggedDriver", {
        headers: { Authorization: "bearer " + token }
      })
      .then(function(response) {
        self.setState({
          name: response.data.first_name,
          email: response.data.email,
          phone: response.data.phone,
          address: response.data.address,
          dateOfBirth: response.data.date_of_birth
        });
      })
      .catch(function(error) {
        console.warn("error", error);
      });
  }
  updateProfile = () => {
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nogard.Views.UpdateProfile",
              options: {
                topBar: {
                  visible: false,
                  height: 0
                }
              }
            }
          }
        ]
      }
    });
  };
  goBack = () => {
    Navigation.dismissAllModals();
  };
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <TouchableOpacity
              transparent
              onPress={() => this.goBack()}
              style={{ paddingLeft: 7 }}
            >
              <Icon name="arrow-back" style={{ color: "white" }} />
            </TouchableOpacity>
          </Left>
          <Body>
            <Title>Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ flex: 1, padding: 20 }}>
          <Button block warning onPress={() => this.updateProfile()}>
            <Text info style={{ fontWeight: "600", fontSize: 18 }}>
              {" "}
              Edit Profile{" "}
            </Text>
          </Button>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Name</Text>
            <Item style={styles.itemContent}>
              <Input disabled={true} value={this.state.name} />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Email</Text>
            <Item style={styles.itemContent}>
              <Input disabled={true} value={this.state.email} />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Phone</Text>
            <Item style={styles.itemContent}>
              <Input disabled={true} value={this.state.phone} />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Address</Text>
            <Item style={styles.itemContent}>
              <Input disabled={true} value={this.state.address} />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Date Of Birth</Text>
            <Item style={styles.itemContent}>
              <Input disabled={true} value={this.state.dateOfBirth} />
            </Item>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  height80: {
    height: 80
  },
  margin10: {
    marginTop: 10
  },
  itemHeading: { fontWeight: "500", fontSize: 18, color: "black" },
  itemContent: {
    marginTop: 10,
    borderWidth: 0,
    borderColor: "#7e8af4",
    backgroundColor: "white",
    borderRadius: 4
  }
});
