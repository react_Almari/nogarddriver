import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
  AsyncStorage
} from "react-native";
import {
  Container,
  Header,
  Content,
  ListItem,
  Icon,
  Left,
  Body,
  Right,
  Button,
  Fab,
  Title
} from "native-base";
import { Navigation } from "react-native-navigation";
import MQTT from "sp-react-native-mqtt";

import MapView, { Marker } from "react-native-maps";
import MapViewDirections from "react-native-maps-directions";
import { ConfirmDialog } from "react-native-simple-dialogs";

let mqttClient = null;
const origin = { latitude: 33.6085, longitude: 73.0638 };
const destination = { latitude: 33.6364, longitude: 73.0751 };
const GOOGLE_MAPS_APIKEY = "AIzaSyBTOqh6gE8OEVBzvEQf4HAjJitUNP8hpNY";
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  button: {
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
    borderRadius: 10
    // opacity: 0.3
  }
});
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisibility: false,
      orderRequests: ["Save Mart", "CashnCarry"],
      currentRequest: true,
      currentStore: null,
      currentCoordinate: {
        latitude: 33.6085,
        longitude: 73.0638
      }
    };
  }

  mqttConnect = async token => {
    console.warn("[in mqtt]: ", token);

    mqttClient = await MQTT.createClient({
      uri: "mqtt://iot.eclipse.org:1883",
      clientId: ""
    });
    mqttClient.on("closed", function() {
      console.warn("[mqtt.event.closed]: ");
    });

    mqttClient.on("error", function(msg) {
      console.warn("[mqtt.event.error]: ", msg);
    });

    mqttClient.on("message", function(topic) {
      let data = JSON.parse(JSON.stringify(topic));
      console.log("[mqtt.message]: ",topic);
      console.log("[topic]: ",data);
      
    });

    mqttClient.on("connect", function() {
      console.warn("[connected]: ");
      mqttClient.subscribe(`/delivery/request/${token}`, 0);
    });

    mqttClient.connect();

    // this.setState({ currentStore: null });
  };
  componentDidMount = async () => {
    let token = await AsyncStorage.getItem("token");
    // console.warn("[token] ", token);

    // this.requestAccess();
    if (token !== null) {
      this.mqttConnect(token);
    } else {
      alert("Unauthenticated!");
    }
  };
  requestAccess = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location permission",
          message:
            "App needs access to your location " +
            "so we can show your location."
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.warn(" location type: [] + watch");
        navigator.geolocation.getCurrentPosition(
          position => {
            console.warn("positionUpdated: ", position);
            this.setState({
              currentCoordinate: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              }
            });
          },
          error => {
            console.warn("error", error);
          },
          { timeout: 100000, enableHighAccuracy: true, maximumAge: 3600000 }
        );
        navigator.geolocation.watchPosition(
          position => {
            console.warn("positionWatching: ", position);
            this.setState({
              currentCoordinate: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude
              }
            });
          },
          error => {
            console.warn("error", error);
          },
          {
            timeout: 100000,
            enableHighAccuracy: true,
            maximumAge: 1000,
            distanceFilter: 10
          }
        );

        console.warn("inside location end");
      } else {
        console.log("Location permission denied");
      }
    } catch (err) {
      console.warn("[out err]", err);
    }
  };
  publish = () => {
    if (mqttClient !== null) {
      mqttClient.publish("driver", "Driver App", 0, false);
      console.warn("sent");
    }
  };
  startDriving = obj => {
    // this.setState({ currentRequest: true, currentStore: obj });
    Navigation.showModal({
      stack: {
        children: [
          {
            component: {
              name: "nogard.Views.Map",
              id: "map",
              options: {
                topBar: {
                  visible: false,
                  height: 0
                }
              }
            }
          }
        ]
      }
    });
  };
  visibleDrawer = () => {
    Navigation.mergeOptions("home", {
      sideMenu: {
        left: {
          visible: true
        }
      }
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <MapView
          ref={ref => (this.mapView = ref)}
          style={styles.map}
          region={{
            latitude: this.state.currentCoordinate.latitude,
            longitude: this.state.currentCoordinate.longitude,
            latitudeDelta: 0.011,
            longitudeDelta: 0.0141
          }}
        >
          <Marker
            coordinate={this.state.currentCoordinate}
            title={"You"}
            description={"name and num"}
          >
            <Image
              source={require("../../../assets/taxi.png")}
              resizeMode={"contain"}
              style={{ height: 45, width: 45, borderRadius: 30 }}
            />
          </Marker>
        </MapView>
        <View
          style={{
            position: "absolute",
            top: 10,
            left: 10,
            right: 10,
            bottom: 10
          }}
        >
          <Header style={{ backgroundColor: "rgb(63, 81, 181)" }}>
            <Left>
              <TouchableOpacity
                transparent
                onPress={() => this.visibleDrawer()}
                style={{ paddingLeft: 7 }}
              >
                <Icon name="menu" style={{ color: "white" }} />
              </TouchableOpacity>
            </Left>
            <Body>
              <Title>NOGARD APP</Title>
            </Body>
            <Right />
          </Header>
          <Fab
            direction="up"
            style={{ backgroundColor: "#5067FF" }}
            position="bottomRight"
            onPress={() => this.publish()}
          >
            <Icon name="share" />
          </Fab>
        </View>
        <ConfirmDialog
          title="Confirm Dialog"
          message="Are you sure about that?"
          visible={this.state.modalVisibility}
          onTouchOutside={() => this.setState({ modalVisibility: false })}
          positiveButton={{
            title: "YES",
            onPress: () => alert("Yes touched!")
          }}
          negativeButton={{
            title: "NO",
            onPress: () => alert("No touched!")
          }}
        />
      </View>
    );
  }
}
