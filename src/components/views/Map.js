import MapView, { Marker } from "react-native-maps";
import React, { Component } from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import MapViewDirections from "react-native-maps-directions";
import { Fab, Icon, Header, Left, Body, Button, Title } from "native-base";
const origin = { latitude: 33.6085, longitude: 73.0638 };
const destination = { latitude: 33.6364, longitude: 73.0751 };
const GOOGLE_MAPS_APIKEY = "AIzaSyBTOqh6gE8OEVBzvEQf4HAjJitUNP8hpNY";
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    flex: 1
  },
  button: {
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
    borderRadius: 10
    // opacity: 0.3
  }
});
export default class Map extends React.Component {
  state = { ripple: false, driver: false };

  loadRipple = () => {};
  render() {
    // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
    return (
      <View style={styles.container}>
        
        <MapView
          ref={ref => (this.mapView = ref)}
          style={styles.map}
          region={{
            latitude: 33.6085,
            longitude: 73.0638,
            latitudeDelta: 0.011,
            longitudeDelta: 0.0141
          }}
        >
          <Marker
            coordinate={{
              latitude: 33.6085,
              longitude: 73.0638
            }}
            title={"You"}
            description={"name and num"}
          >
            <Image
              source={require("../../../assets/taxi.png")}
              resizeMode={"contain"}
              style={{ height: 45, width: 45, borderRadius: 30 }}
            />
          </Marker>
          <Marker
            coordinate={{
              latitude: 33.6364,
              longitude: 73.0751
            }}
            title={"Shop"}
            description={"Your Location"}
          />
          <MapViewDirections
            origin={origin}
            destination={destination}
            apikey={GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor="hotpink"
          />
        </MapView>
        <View
          style={{
            position: "absolute",
            top:10,
            left:10,
            right: 10,
            bottom:10
          }}
        >
          <Header style={{}}>
          <Left>
            <Button transparent>
              <Icon name="menu" />
            </Button>
          </Left>
          <Body>
            <Title>Nogart</Title>
          </Body>
        </Header>
          <Fab
            direction="up"
            style={{ backgroundColor: "#5067FF" }}
            position="bottomRight"
          >
            <Icon name="share" />
          </Fab>
        </View>
      </View>
    );
  }
}
