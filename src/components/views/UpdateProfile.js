/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { StyleSheet, Text, View, AsyncStorage } from "react-native";
import {
  Item,
  Input,
  Container,
  Header,
  Left,
  Body,
  Content,
  Button,
  Right,
  Title
} from "native-base";
import { Navigation } from "react-native-navigation";
const axios = require("axios");
export default class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      address: "",
      dateOfBirth: ""
    };
  }
  async componentDidMount() {
    const self = this;
    const token = await AsyncStorage.getItem("token");
    axios.defaults.headers.common = { Authorization: "bearer " + token };
    axios
      .get("http://192.168.31.42:3100/v1/drivers/loggedDriver")
      .then(function(response) {
        self.setState({
          name: response.data.first_name,
          email: response.data.email,
          phone: response.data.phone,
          address: response.data.address,
          dateOfBirth: response.data.date_of_birth
        });
      })
      .catch(function(error) {
        console.warn("error", error);
      });
  }
  updateProfile = () => {
    axios
      .put("http://192.168.31.42:3100/v1/drivers/loggedDriver", {
        first_name: this.state.name,
        email: this.state.email,
        phone: this.state.phone,
        address: this.state.address,
        date_of_birth: this.state.dateOfBirth
      })
      .then(function(response) {
        alert("Profile Updated");
      })
      .catch(function(error) {
        console.warn("error", error);
      });
  };

  inputHandler = (id, e) => {
    this.setState({ [id]: e });
  };
  render() {
    return (
      <Container>
        <Header>
          <Left />
          <Body>
            <Title>Update Profile</Title>
          </Body>
          <Right />
        </Header>
        <Content style={{ flex: 1, padding: 20 }}>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Name</Text>
            <Item style={styles.itemContent}>
              <Input
                value={this.state.name}
                onChangeText={e => this.inputHandler("name", e)}
              />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Email</Text>
            <Item style={styles.itemContent}>
              <Input
                value={this.state.email}
                onChangeText={e => this.inputHandler("email", e)}
              />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Phone</Text>
            <Item style={styles.itemContent}>
              <Input
                value={this.state.phone}
                onChangeText={e => this.inputHandler("phone", e)}
              />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Address</Text>
            <Item style={styles.itemContent}>
              <Input
                value={this.state.address}
                onChangeText={e => this.inputHandler("address", e)}
              />
            </Item>
          </View>
          <View style={[styles.height80, styles.margin10]}>
            <Text style={styles.itemHeading}>Date Of Birth</Text>
            <Item style={styles.itemContent}>
              <Input
                value={this.state.dateOfBirth}
                onChangeText={e => this.inputHandler("dateOfBirth", e)}
              />
            </Item>
          </View>
          <Button block warning onPress={() => this.updateProfile()}>
            <Text info style={{ fontWeight: "600", fontSize: 18 }}>
              Update Profile
            </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  height80: {
    height: 80
  },
  margin10: {
    marginTop: 10
  },
  itemHeading: { fontWeight: "500", fontSize: 18, color: "black" },
  itemContent: {
    marginTop: 10,
    borderWidth: 0,
    borderColor: "#7e8af4",
    backgroundColor: "white",
    borderRadius: 4
  }
});
