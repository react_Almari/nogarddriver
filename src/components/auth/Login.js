import React, { Component } from "react";
import {
  Container,
  Button,
  Text,
  Item,
  Body,
  Content,
  Form,
  Card,
  CardItem,
  Input,
  Label
} from "native-base";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  AsyncStorage,
  ToastAndroid
} from "react-native";

import { Navigation } from "react-native-navigation";
const axios = require("axios");
export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "ali@gmail.com", password: "ali12345" };
  }
  inputHandler = (id, e) => {
    this.setState({ [id]: e });
  };
  loginHandler = (email, password) => {
    console.warn("pressed", email, password);
    axios
      .post("http://192.168.31.42:3100/v1/auth/login/driver", {
        email,
        password
      })
      .then(async function(response) {
        console.log("resp", response);
        await AsyncStorage.setItem("token", response.data.driver.id);
        // console.warn("Async", await AsyncStorage.getItem("token"));
        ToastAndroid.show("SignIn Success", ToastAndroid.SHORT);
        Navigation.setRoot({
          root: {
            sideMenu: {
              left: {
                component: {
                  name: "nogard.UI.Drawer"
                }
              },
              center: {
                component: {
                  name: "nogard.Views.Home",
                  id: "home"
                }
              }
            }
          }
        });
      })
      .catch(function(error) {
        console.warn("error", error);
      });
  };
  render() {
    return (
      <Container style={styles.container}>
        <Form style={styles.form}>
          <Label style={styles.label}>
            nogard
            <Label style={styles.labelInside}>A</Label>pp
          </Label>
        </Form>
        <Form style={styles.form2}>
          <Content padder>
            <View style={{ padding: 40 }}>
              <Card>
                <CardItem header button style={{ justifyContent: "center" }}>
                  <Text style={styles.fontSize}>Sign In</Text>
                </CardItem>
                <CardItem button>
                  <Body>
                    <View style={styles.form}>
                      <Item style={styles.inputField}>
                        <Input
                          autoCapitalize="none"
                          placeholder="Email Address"
                          id="email"
                          value={this.state.email}
                          onChangeText={e => this.inputHandler("email", e)}
                        />
                      </Item>
                      <Item style={styles.inputField}>
                        <Input
                          autoCapitalize="none"
                          placeholder="Password"
                          id="password"
                          value={this.state.password}
                          onChangeText={e => this.inputHandler("password", e)}
                        />
                      </Item>
                      <Button
                        block
                        style={{ backgroundColor: "#707DF4", marginTop: 5 }}
                        onPress={() =>
                          this.loginHandler(
                            this.state.email,
                            this.state.password
                          )
                        }
                      >
                        <Text>Sign In</Text>
                      </Button>
                    </View>

                    <CardItem button style={styles.signUp}>
                      <TouchableOpacity>
                        <Text
                          style={{
                            textDecorationLine: "underline"
                          }}
                        >
                          SignUp
                        </Text>
                      </TouchableOpacity>
                    </CardItem>
                  </Body>
                </CardItem>
              </Card>
            </View>
          </Content>
        </Form>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "#7e8af4"
  },
  form: { flex: 1, alignItems: "center", justifyContent: "center" },
  label: { color: "white", fontSize: 30, fontWeight: "bold" },
  labelInside: { fontSize: 60 },
  fontSize: { fontSize: 22 },
  form2: {
    flex: 2,
    flexDirection: "row",
    alignItems: "flex-start",
    justifyContent: "center"
  },
  inputField: {
    borderWidth: 0,
    borderColor: "#7e8af4",
    backgroundColor: "white",
    borderRadius: 4
  },
  signUp: {
    justifyContent: "center",
    flexDirection: "column",
    width: "100%"
  }
});
